[ "Name", "User URL", "Membership type", "URL", "Access" ]
,
(
  .data.users.nodes []
  | .name as $username
  | .webUrl as $userurl
  | (
      [
        .groupMemberships.nodes[]
        | [ "group", .group.webUrl, .accessLevel.stringValue ]
      ]
      +
      [
        .projectMemberships.nodes[]
        | [ "project", .project.webUrl, .accessLevel.stringValue ]
      ]
    ) []
  | ( [ $username, $userurl ] + .)
)
| @csv
