# Experiment Gitlab Graphql

(I think this requires signing in to gitlab.com first.)

In https://gitlab.com/-/graphql-explorer:

* copy-paste the contents of [example-user-memberships-query.graphql](example-user-memberships-query.graphql) into the query textarea
* click "QUERY VARIABLES" to open its view
* copy-paste the contents of [example-user-memberships-input.json](example-user-memberships-input.json) into the query variables text area and change the value for "username"
* click the play button
* copy-paste the results into a file
* `cat results.json | jq -r -f example-user-memberships-to-csv.jq > results.csv`
